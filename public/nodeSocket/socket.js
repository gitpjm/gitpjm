var WebSocketServer = require("ws").Server;

const wss = new WebSocketServer({ port: 30001 });

let obj = null;
let lastUserId = 0;
const users = new Map();

wss.on("connection", function connection(ws) {

  const userId = lastUserId++;
  users.set(userId, ws);
  console.log(userId, "连接成功了");
  ws.on("error", ()=>{
    console.log(userId,'连接出错了')
    users.delete(userId);
  });
  ws.on("message", function message(data) {
    obj = data + "";
    console.log("received: %s", userId, obj);
    const client = users.get(userId);

    users.forEach((value, key) => {
      if (key != userId) {
        if (value) {
          value.send(obj);
        } else {
          console.log(`User with ID ${key} not found.`);
        }
      }
    });
    // wss.clients.forEach(function each(client) {
    //   //wss.clients存放着连接到我们服务器所有的客户端,通过遍历，将客户端的消息转发给其他客户端,从而实现群聊
    //   client.send(obj, { binary: false });
    // });
  });
  ws.on('close', () => {
    users.delete(userId);
    console.log(userId,'连接关闭了');
  });



});

console.log("--WebSocket-------------");
console.log("WebSocket address: ws://127.0.0.1:30001");
console.log("WebSocket has started.");
console.log("------------------------");

